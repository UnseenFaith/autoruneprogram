const cheerio = require('cheerio');
const fetch = require('node-fetch');
const Page = require ('./Page');

class Source {
    constructor(summoner, name, baseURL) {
        this.summoner = summoner;

        this.name = name || this.constructor.name;

        this.baseURL = baseURL;

        this.selectors = [];
    }

    async fetch(champion, role) {
        const html = await this.request(this.url(champion, role));
        const $ = cheerio.load(html);
        const runes = [];
        for (const selector of this.selectors.entries()) {
            runes.push(this.parse($, selector));   
        }
        return new Page(this, champion, role, runes);
    }

    parse($, [index, selector]) {
        throw new Error(`${this.name} did not implement parse function.`);
    }

    url(champion, role) {
        throw new Error(`${this.name} did not implement a URL function.`);
    }

    request(...args) {
        return fetch(...args)
            .then(res => res.text())
            .catch(err => { throw err; });
    }
}

module.exports = Source;