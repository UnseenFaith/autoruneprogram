const Riot = require('../util/RiotSocket');
const fetch = require('node-fetch');

class Routes {
    constructor(summoner) {
        Object.defineProperty(this, 'app', { value: summoner.app });
        Object.defineProperty(this, 'summoner', { value: summoner });
        Object.defineProperty(this, 'initialized', { value: false, writable: true });

        this.url = null;
        this.headers = null;
        this.ws = null;
    
    }

    async ['/lol-champ-select/v1/session'](data) {
        if (!this.initialized) await this.initialize();
        if (!data || data.timer.phase !== 'FINALIZATION') return;
        if (this.summoner.pages.set) return;
        this.summoner.pages.set = true;
        const { championId, assignedPosition } = data.myTeam.find(p => p.summonerId = this.summoner.id);
        const champion = await this.request(`/lol-champions/v1/inventories/${this.summoner.id}/champions/${championId}`).then(r => r.json());
        await this.summoner.pages.create(champion, assignedPosition);
    }

    async ['/lol-gameflow/v1/session'](data) {
        if (!data) return;
        // Phases
        // None = Game is finished and there is no lobby a matchmaking queue.
        // Matchmaking = Queued up for game, occurs when champ select is dodged or you start a queue.
        if ((data.phase === 'Matchmaking' && this.summoner.pages.id !== null) || data.phase === 'None') {
            if (this.summoner.pages.id) await this.request(`/lol-perks/v1/pages/${this.summoner.pages.id}`, { method: "DELETE" });
            this.summoner.pages.id = null;
            this.summoner.pages.set = false;
        }
    }

    async initialize() {
        this.initialized = true;
        await Promise.all([
            this.summoner.initialize(),
            this.summoner.pages.initialize(),
        ]);

        console.log('Routes Initialized');
    }

    request(url, options) {
        return fetch(`${this.url}${url}`, { headers: this.headers, ...options })
                .catch(e => { throw e; });
    }

    patch({ protocol, address, port, username, password }) {
        this.ws = new Riot(`wss://${username}:${password}@${address}:${port}/`);
        this.ws.on('open', () => {
            this.ws.subscribe('OnJsonApiEvent', (payload) => {
                return this[payload.uri] ? this[payload.uri](payload.data) : false;
            });
            console.log('Websocket connected and logged in.');
        });

        this.url = `${protocol}://${address}:${port}`;
        this.headers = {
            'Content-Type': 'application/json',
            'Authorization': `Basic ${Buffer.from(`${username}:${password}`).toString('base64')}`
        };
    }
}

module.exports = Routes;