class Page {
    constructor(source, champion, role, runes) {
        this.isActive = true;
        this.current = true;
        this.name = `${source.name} ${champion.name} ${role ? role : ""}`;
        this.primaryStyleId = runes[0];
        this.subStyleId = runes[1];
        this.selectedPerkIds = runes.slice(2);
    }

    toString() {
        return JSON.stringify(this);
    }
}

module.exports = Page;