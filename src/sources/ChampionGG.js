const Source = require('../lib/structures/Source');

class ChampionGG extends Source {
    constructor(summoner) {
        super(summoner, 'ChampionGG', 'https://champion.gg');

        this.selectors = [
            "#primary-path > div:nth-child(1) > div.Slot__RightSide-bGHpkV.iSYqxs > div > div",
            "#secondary-path > div.Slot__Block-epLguL.iEzUxo > div.Slot__RightSide-bGHpkV.iSYqxs > div:nth-child(1) > div > div",
            "#primary-path > div:nth-child(2) > div.Slot__RightSide-bGHpkV.iSYqxs > div:nth-child(3) > div > div",
            "#primary-path > div:nth-child(3) > div.Slot__RightSide-bGHpkV.iSYqxs > div:nth-child(1) > div > div",
            "#primary-path > div:nth-child(4) > div.Slot__RightSide-bGHpkV.iSYqxs > div:nth-child(1) > div > div",
            "#primary-path > div:nth-child(5) > div.Slot__RightSide-bGHpkV.iSYqxs > div:nth-child(1) > div > div",
            "#secondary-path > div:nth-child(2) > div:nth-child(1) > div.Slot__RightSide-bGHpkV.iSYqxs > div.Description__Block-bJdjrS.hGZpqL > div",
            "#secondary-path > div:nth-child(2) > div:nth-child(2) > div.Slot__RightSide-bGHpkV.iSYqxs > div > div",
            "#secondary-path > div:nth-child(3) > div:nth-child(1) > div.Slot__RightSide-bGHpkV.iSYqxs.iLoveCSS > div.Description__Block-bJdjrS.hGZpqL.statShardsOS > div",
            "#secondary-path > div:nth-child(3) > div:nth-child(2) > div.Slot__RightSide-bGHpkV.iSYqxs.iLoveCSS > div > div",
            "#secondary-path > div:nth-child(3) > div:nth-child(3) > div.Slot__RightSide-bGHpkV.iSYqxs.iLoveCSS > div.Description__Block-bJdjrS.hGZpqL.statShardsOS > div"
        ];

        this.slots = {
            'Scaling Health': 5001,
            'Armor': 5002,
            'Magic Resist': 5003,
            'Attack Speed': 5005,
            'Scaling Cooldown Reduction': 5007,
            'Adaptive Force': 5008
        }
    }

    parse($, [index, selector]) {
        let rune;
        $(selector).each((i, e) => {
            // i === 0 Highest Win Rate Runes
            // i === 1 Most Frequent Runes
            if (i === 1) {
                rune = $(e).html();
                if (index === 0 || index === 1) rune = this.summoner.pages.styles.find(style => style.name.toLowerCase() === rune.toLowerCase()).id;
                else if ([8,9,10].includes(index)) rune = this.slots[rune];
                else rune = this.summoner.pages.runes.find(run => run.name.toLowerCase() === rune.toLowerCase()).id;
            }
        });
        return rune;
    }

    url(champion) {
        const name = champion.alias.toLowerCase() === champion.name.toLowerCase() ? champion.name : champion.alias;
        return `${this.baseURL}/champion/${name}`;
    }

}

module.exports = ChampionGG;